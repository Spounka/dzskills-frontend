import React from 'react';
import TeacherDashboardLayout from '../layout';

function TeacherCourses() {
    return <TeacherDashboardLayout topbar_title={'teacher'} />;
}

export default TeacherCourses;
