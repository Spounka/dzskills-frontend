import { Button, Card, Divider, Typography, useTheme } from '@mui/material';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Image from 'mui-image';
import { useEffect, useState } from 'react';
import { useMutation, useQuery } from 'react-query';
import { useNavigate, useParams } from 'react-router-dom';
import pdf_icon from '../../assets/png/pdf@2x.png';
import uploadImg from '../../assets/svg/upload gray.svg';
import AuthenticationTopBar from '../../components/ui/AuthenticationTopBar';
import DownloadSvgIcon from '../../components/ui/DownloadSvgIcon';
import { MainButton } from '../../components/ui/MainButton';
import { getCourse } from '../course/api/getCourse';
import NotFound from '../not-found/NotFound';
import { createOrder, getCurrentReceipt } from './api/createOrder';
import { Receipt } from '../../types/admin_config';

function BuyCourse() {
    const params = useParams();

    if (!params || !params.id) return <Typography>Error</Typography>;

    // @ts-ignore
    if (isNaN(params.id)) return <NotFound />;

    const id: number = parseInt(params.id);
    const theme = useTheme();
    const [fileName, setFileName] = useState<File>();
    const navigate = useNavigate();

    function handleFilechange(event: any) {
        setFileName(event.target.files[0]);
    }

    const query = useQuery({
        queryKey: ['courses', id],
        queryFn: () => getCourse(id),
        staleTime: 1000 * 60 * 60 * 24,
    });

    const mutation = useMutation({
        mutationKey: ['offers', id, 'create'],
        mutationFn: (data: any) => createOrder(data),
    });
    async function submitForm(e: any) {
        e.preventDefault();
        const form = document.querySelector('form');
        if (form) {
            let formData = new FormData(form);
            formData.append('course', id.toString());
            mutation.mutate(formData);
        }
    }
    const [imageLink, setImageLink] = useState<string>('');
    useEffect(() => {
        const getReceipt = async () => await getCurrentReceipt();
        getReceipt()
            .then((data: Receipt) => {
                setImageLink(data.image);
            })
            .catch(err => console.log(err));
    }, []);

    if (query.isError) return <>Error</>;
    if (query.isLoading || query.isFetching) return <>Loading...</>;

    return (
        <Grid
            container
            direction="column"
            spacing={5}
            id={'main grid container'}
            columns={14}
            sx={{
                backgroundColor: 'white',
                maxWidth: '100%',
            }}
        >
            <Grid
                container
                item
                xs={14}
            >
                <AuthenticationTopBar />
            </Grid>

            <Grid
                item
                xs={14}
                container
                sx={{
                    backgroundColor: 'gray.secondary',
                }}
            >
                <Box
                    sx={{
                        display: 'grid',
                        gridTemplateColumns: 'repeat(26, minmax(0, 1fr))',
                        width: '100%',
                        marginBottom: '2rem',
                    }}
                >
                    <Box
                        sx={{
                            display: 'flex',
                            flexDirection: 'column',
                            gap: 2,
                            gridColumnStart: 3,
                            gridColumnEnd: 26,
                        }}
                    >
                        <Typography
                            color={'secondary.dark'}
                            variant={'h6'}
                            fontWeight={600}
                            flexGrow={1}
                            width="100%"
                        >
                            تأكيد الشراء
                        </Typography>
                        <Box
                            display="flex"
                            gap={2}
                        >
                            <Box
                                sx={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    flexBasis: '33%',
                                    height: '100%',
                                    flexShrink: '1',
                                    gap: 4,
                                }}
                            >
                                <Image
                                    src={query.data?.thumbnail || ''}
                                    height={'40%'}
                                />

                                <Box
                                    display={'flex'}
                                    gap={2}
                                    flexDirection={'column'}
                                >
                                    <Box
                                        display={'flex'}
                                        justifyContent={'space-between'}
                                        pl={4}
                                    >
                                        <Typography
                                            color={'gray.main'}
                                            variant={'h6'}
                                            fontWeight={500}
                                        >
                                            السعر
                                        </Typography>
                                        <Typography
                                            color={'secondary.dark'}
                                            variant={'h6'}
                                            fontWeight={500}
                                            sx={{
                                                direction: 'ltr',
                                            }}
                                        >
                                            {query.data?.price} DA
                                        </Typography>
                                    </Box>

                                    <Box
                                        display={'flex'}
                                        justifyContent={'space-between'}
                                        pl={4}
                                    >
                                        <Typography
                                            color={'gray.main'}
                                            variant={'h6'}
                                            fontWeight={500}
                                        >
                                            ملحقات
                                        </Typography>
                                        <Typography
                                            color={'secondary.dark'}
                                            variant={'h6'}
                                            fontWeight={500}
                                            sx={{
                                                direction: 'ltr',
                                            }}
                                        >
                                            {query.data?.price} DA
                                        </Typography>
                                    </Box>
                                </Box>

                                <Divider />
                                <Box
                                    display={'flex'}
                                    justifyContent={'space-between'}
                                    pl={4}
                                >
                                    <Typography
                                        color={'gray.main'}
                                        variant={'h6'}
                                        fontWeight={500}
                                    >
                                        المجموع
                                    </Typography>
                                    <Typography
                                        color={'secondary.dark'}
                                        variant={'h6'}
                                        fontWeight={500}
                                        sx={{
                                            direction: 'ltr',
                                        }}
                                    >
                                        {query.data?.price} DA
                                    </Typography>
                                </Box>
                            </Box>
                            <Card
                                elevation={0}
                                sx={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    flexBasis: '60%',
                                    width: '100%',
                                    height: 'min-content',
                                    // flexShrink: '1',
                                    borderRadius: theme.spacing(),
                                    py: 9,
                                    px: 12,
                                    gap: 6,
                                }}
                            >
                                <Box
                                    display={'flex'}
                                    justifyContent={'space-between'}
                                    alignItems={'center'}
                                >
                                    <Typography
                                        color={'gray.main'}
                                        variant={'subtitle2'}
                                        fontWeight={400}
                                        maxWidth={'60%'}
                                    >
                                        يرجى تحميل معلومات الدفع الخاصة بالموقع
                                        من هنا
                                    </Typography>
                                    <a
                                        download
                                        target="_blank"
                                        href={imageLink}
                                        style={{
                                            textDecoration: 'none',
                                        }}
                                    >
                                        <MainButton
                                            sx={{
                                                borderRadius: theme.spacing(),
                                                gap: 2,
                                            }}
                                            {...{
                                                size: 'large',
                                                endIcon: (
                                                    <DownloadSvgIcon
                                                        {...{
                                                            width: theme.spacing(
                                                                2
                                                            ),
                                                            height: theme.spacing(
                                                                2
                                                            ),
                                                        }}
                                                    />
                                                ),
                                            }}
                                            text={'تحميل'}
                                            color={theme.palette.primary.main}
                                        />
                                    </a>
                                </Box>

                                <form
                                    onSubmit={submitForm}
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                        width: '100%',
                                        height: 'min-content',
                                        // flexShrink: '1',
                                        borderRadius: theme.spacing(),
                                        paddingTop: 9,
                                        paddingBottom: 9,
                                        paddingLeft: 12,
                                        paddingRight: 12,
                                        gap: theme.spacing(6),
                                    }}
                                >
                                    <Box
                                        display={'flex'}
                                        justifyContent={'space-between'}
                                        alignItems={'center'}
                                        gap={9}
                                    >
                                        <Typography
                                            color={'gray.main'}
                                            variant={'subtitle2'}
                                            fontWeight={400}
                                            maxWidth={'60%'}
                                        >
                                            عند إكمال الدفع يرجى ارفاق الوصل
                                            حتى نتمكن من تأكيد دفعكم . عملية
                                            التأكيد بين 24/48 ساعة
                                            <br />
                                            يمكنكم العودة لهذه الصفحة عبر
                                            الولوج الى صفحة{' '}
                                            <strong>
                                                {' '}
                                                الطلبات و الفواتير{' '}
                                            </strong>
                                            من اعدادات حسابكم
                                        </Typography>
                                        <Button
                                            component={'label'}
                                            variant={'contained'}
                                            sx={{
                                                flexGrow: '1',
                                                bgcolor: 'gray.secondary',
                                                color: 'gray.main',
                                                ':hover': {
                                                    bgcolor: 'gray.secondary',
                                                    color: 'gray.main',
                                                },
                                            }}
                                            // endIcon={ }
                                        >
                                            رفع
                                            <img
                                                src={uploadImg}
                                                style={{
                                                    width: '10%',
                                                    height: 'auto',
                                                    padding: 0,
                                                    // marginLeft: theme.spacing(),
                                                    marginRight:
                                                        theme.spacing(),
                                                }}
                                            />
                                            <input
                                                // hidden
                                                style={{
                                                    width: 1,
                                                    height: 1,
                                                }}
                                                required={true}
                                                onChange={handleFilechange}
                                                name={'payment.receipt'}
                                                accept={'*'}
                                                type="file"
                                            />
                                        </Button>
                                    </Box>
                                    <Box
                                        sx={{
                                            bgcolor: 'gray.secondary',
                                            width: '100%',
                                            height: 'auto',
                                            borderRadius: theme.spacing(),
                                            py: theme.spacing(2),
                                            px: theme.spacing(3),
                                            display: 'flex',
                                            alignItems: 'center',
                                            gap: 2,
                                        }}
                                    >
                                        <img
                                            src={pdf_icon}
                                            style={{
                                                height: theme.spacing(4),
                                            }}
                                        />

                                        <Typography
                                            variant={'subtitle2'}
                                            color={'gray.main'}
                                        >
                                            {fileName?.name}
                                        </Typography>
                                    </Box>
                                    <Box
                                        display={'flex'}
                                        justifyContent={'space-between'}
                                    >
                                        <MainButton
                                            type={'submit'}
                                            text={'شراء الآن'}
                                            color={theme.palette.primary.main}
                                            // {...{ onClick: () => navigate('..') }}
                                        />

                                        <MainButton
                                            text={'إلغاء الطلب'}
                                            color={theme.palette.error.main}
                                            {...{
                                                onClick: () => navigate('..'),
                                            }}
                                        />
                                    </Box>
                                </form>
                            </Card>
                        </Box>
                    </Box>
                </Box>
            </Grid>
        </Grid>
    );
}
export default BuyCourse;
