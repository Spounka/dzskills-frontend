import { Typography } from '@mui/material';

function NotFound() {
    return (
        <Typography
            fontFamily={'Montserrat Arabic'}
            variant="h4"
            fontWeight={300}
        >
            404 Page Not found
        </Typography>
    );
}

export default NotFound;
