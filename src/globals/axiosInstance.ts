import axios from 'axios';

const token = localStorage.getItem('access');
const axiosInstance = axios.create({
    baseURL: import.meta.env.VITE_HOST || 'https://dzskills.com/api',
    headers: token
        ? {
              'Content-Type': 'multipart/form-data',
              Authorization: `Bearer ${token}`,
          }
        : {
              'Content-Type': 'multipart/form-data',
          },
});

export default axiosInstance;
