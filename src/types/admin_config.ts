export type Receipt = {
    id: number;
    image: string;
    count: number;
    is_current: Boolean;
};
