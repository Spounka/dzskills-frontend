import { Route, Routes } from 'react-router-dom';
import HashtagsAndCategories from './pages/admin-panel/categories-hashtags';
import CourseDetails from './pages/admin-panel/course-details';
import AdminCourses from './pages/admin-panel/courses';
import AdminLandingPage from './pages/admin-panel/landing-page';
import PaymentManagement from './pages/admin-panel/payment-management';
import AddAdmin from './pages/admin-panel/settings/add-admin';
import AdminPersonalDetails from './pages/admin-panel/settings/edit';
import LandingPageSettings from './pages/admin-panel/settings/landing-page';
import Receipts from './pages/admin-panel/settings/receipts';
import UserDetails from './pages/admin-panel/user-details';
import UserManagement from './pages/admin-panel/user-management';
import Authenticate from './pages/authenticate/Authenticate';
import BuyCourse from './pages/buy-course';
import ViewCourse from './pages/course';
import CoursesPage from './pages/courses-page';
import EditProfile from './pages/edit-profile';
import GoogleCallbackView from './pages/google-callback';
import LandingPage from './pages/landing-page';
import Logout from './pages/logout/indext';
import ContactTeacher from './pages/messages';
import NotFound from './pages/not-found/NotFound';
import PasswordForgotten from './pages/password-forgotten';
import Profile from './pages/profile';
import Invoices from './pages/profile-invoices';
import EmailSendPage from './pages/send-email';
import ContactSupport from './pages/support/contact';
import TeacherAddCourse from './pages/teacher-dashboard/add-course';
import TeacherCourses from './pages/teacher-dashboard/courses';
import TeacherLandingPage from './pages/teacher-dashboard/landing-page';
import EmailValidationPage from './pages/validate-email';
import ViewCertificate from './pages/view-certificate';
import WatchCourse from './pages/view-course';

function App() {
    return (
        <Routes>
            {/* User Application */}
            <Route
                path=""
                element={<LandingPage />}
            />
            <Route
                path="login"
                element={<Authenticate startPanel={1} />}
            />
            <Route path="register">
                <Route
                    path=""
                    element={<Authenticate startPanel={0} />}
                />
                <Route
                    path="verify-email/"
                    element={<EmailSendPage />}
                />
                <Route
                    path="verify-email/:key/"
                    element={<EmailValidationPage />}
                />
                <Route
                    path="google"
                    element={<GoogleCallbackView />}
                />
            </Route>

            <Route
                path="logout"
                element={<Logout />}
            />
            <Route path="password">
                <Route path="reset">
                    <Route
                        path=""
                        element={<PasswordForgotten />}
                    />
                    <Route
                        path="confirm"
                        element={<PasswordForgotten stage={1} />}
                    />
                </Route>
            </Route>
            <Route path="/profile">
                <Route
                    path=""
                    element={<Profile />}
                />
                <Route
                    path="edit"
                    element={<EditProfile />}
                />
                <Route
                    path="cart"
                    element={<Invoices />}
                />
            </Route>
            <Route path="contact">
                <Route
                    path=""
                    element={<ContactSupport />}
                />
            </Route>
            <Route path="/courses">
                <Route
                    path=""
                    element={<CoursesPage />}
                />
                <Route path=":id">
                    <Route
                        path=""
                        element={<ViewCourse />}
                    />
                    <Route
                        path="watch"
                        element={<WatchCourse />}
                    />
                    <Route
                        path="certificate"
                        element={<ViewCertificate />}
                    />
                    <Route
                        path="buy"
                        element={<BuyCourse />}
                    />
                    <Route
                        path="contact"
                        element={<ContactTeacher />}
                    />
                </Route>
            </Route>

            {/* Teacher Dashboard */}
            <Route path="/dashboard">
                <Route path="teacher">
                    <Route
                        path={''}
                        element={<TeacherLandingPage />}
                    />
                    <Route path="courses">
                        <Route
                            path=""
                            element={<TeacherCourses />}
                        />
                        <Route
                            path="add"
                            element={<TeacherAddCourse />}
                        />
                    </Route>
                    <Route
                        path="messages"
                        element={<span>Messages</span>}
                    />
                    <Route
                        path="statistics"
                        element={<span>Statistics</span>}
                    />
                    <Route
                        path="accounts"
                        element={<span>Account</span>}
                    />
                </Route>
            </Route>

            {/* Admin Panel */}
            <Route path="/admin">
                <Route
                    path=""
                    element={<AdminLandingPage />}
                />
                <Route
                    path="courses"
                    element={<AdminCourses />}
                />
                <Route
                    path="courses/:id/"
                    element={<CourseDetails />}
                />
                <Route
                    path="hashtags-categories"
                    element={<HashtagsAndCategories />}
                />
                <Route
                    path="payments"
                    element={<PaymentManagement />}
                />
                <Route
                    path="users"
                    element={<UserManagement />}
                />
                <Route
                    path="users/:id/"
                    element={<UserDetails />}
                />
                <Route
                    path="settings/"
                    element={<AdminPersonalDetails />}
                />
                <Route path="settings">
                    <Route
                        path="add-admin"
                        element={<AddAdmin />}
                    />
                    <Route
                        path="receipts"
                        element={<Receipts />}
                    />
                    <Route
                        path={'landing-page'}
                        element={<LandingPageSettings />}
                    />
                </Route>
            </Route>

            <Route
                path="*"
                element={<NotFound />}
            />
        </Routes>
    );
}

export default App;
